\chapter{El laplacià. Funcions harmòniques}
\section{Introducció}
Les solucions de l'equació del calor $u_t - \Delta u = f(x)$ convergiran,
quan $t \to \infty$, cap a la solució estacionària del problema, és a dir, la solució de
\[
	\begin{cases}
		-\Delta v = f(x) & \Omega \subset \real^n \\
		\text{condicions de vora} &
	\end{cases}
\]
Això ens indueix de forma natural a estudiar aquesta nova equació, coneguda com
l'equació de Poisson o, en el cas homogeni $f \equiv 0$, com l'equació de Laplace.
\begin{defi}[funció!harmònica]
	Sigui $\Omega \subset \real^n$ obert, $u \in \C^2(\Omega)$. Direm que
	$u$ és harmònica a $\Omega$ si
	\[
		\Delta u = u_{x_1 x_1} + ... + u_{x_n x_n} = 0 \text{ a }\Omega.
	\]
	De la mateixa manera, direm que $u$ és subharmònica a $\Omega$ si $-\Delta u \leq 0$, 
	i direm que $u$ és superharmònica a $\Omega$ si $-\Delta u \geq 0$.
\end{defi}
\begin{obs}
	Tota funció convexa és subharmònica. Recordem que en un cert $\Omega \subset \real^n$ convex,
	una funció $u$ és convexa si i només si es compleix alguna de les següents condicions:
	\begin{itemize}
		\item A cada punt de la gràfica, l'hiperplà tangent per aquell punt queda
		per sota de la gràfica.
		\item Restringida a qualsevol segment és una funció convexa d'una variable.
		\item Per a tot $x,y \in \Omega$, es compleix $u(\frac{x+y}{2}) \leq \frac{u(x) + u(y)}{2}$.
		\item Per a tot $x \in \Omega$, es compleix $D^2u(x) \geq 0$.
	\end{itemize}
	Per tant, per ser $D^2u$ definida positiva, tenim
	\[
		\Delta u = \tr D^2u \geq 0.
	\]
\end{obs}
Un exemple molt senzill de funcions harmòniques són les funcions afins $u(x) = a\cdot x + b$,
amb $a\in \real^n$ i $b \in \real$. En dimensió 1, de fet, aquestes són totes les funcions harmòniques 
(per integració directa de l'equació). En dimensió 2, ens és convenient fer la identificació
\begin{gather*}
\real^2 \cong \mathbb{C} \\
(x,y) \leftrightarrow z = x+iy.
\end{gather*}
Recordem que una funció $\varphi \colon \Omega \subset \mathbb{C} \to \mathbb{C}$ és holomorfa o analítica
si és $\C^1$ com a funció de variable complexa, i que tota funció holomorfa és automàticament $\C^{\infty}$.
També recordem que podem escriure $\varphi = u +iv$, sent $u,v\colon \Omega \to \real$
les parts real i imaginària de $\varphi$. En particular,
\begin{prop}[Equacions de Cauchy-Riemann]
	Si $\varphi = u+iv$ és holomorfa, llavors $u_x = v_y$ i $u_y = -v_x$.
\end{prop}
\begin{col}
	Les parts real i imaginària $u$ i $v$ d'una funció holomorfa són $\C^{\infty}$
	i harmòniques a $\Omega$.
\end{col}
\begin{proof}
	\begin{gather*}
		\Delta u = u_{xx} + u_{yy} = (v_y)_x + (-v_x)_y = v_{yx} - v_{xy} = 0. \\
		\Delta v = v_{xx} + v_{yy} = (-u_y)_x + (u_x)_y = -u_{yx} + u_{xy} = 0.
	\end{gather*}
\end{proof}
Recíprocament, també és cert que si $u \colon \Omega \to \real$ és harmònica en 
$\Omega$ simplement connex, llavors existeix una altra funció harmònica $v \colon \Omega \to \real$
(l'harmònica conjugada) de manera que $u + iv$ és holomorfa. En tot cas, ens serà suficient
trobar funcions holomorfes per tal d'obtenir exemples de funcions harmòniques de dues variables.
Per exemple,
\[
	\varphi(z) = z^2 = (x+iy)^2 = (x^2-y^2) + i(2xy), 
\]
i així, tant $x^2 + y^2$ com $2xy$ són funcions harmòniques. Podem fer
el mateix per a $z^k$, $k = 1,2,3,...$ per a obtenir una família de polinomis harmònics.
Si ho fem en coordenades polars,
\[
	z^k = (re^{i \theta})^k = r^k e^{ik\theta} = (r^k cos (k \theta)) + i (r^k \sin (k\theta)),
\]
i per tant $r^k \cos(k \theta)$ i $r^k \sin(k \theta)$ són harmòniques.
\begin{ej}
	Demostreu que el laplacià en coordenades polars és
	\[
		\Delta u = u_{rr} + \frac{u_r}{r} + \frac{u_{\theta\theta}}{r^2} =
		 \frac{1}{r} (ru_r)_r + \frac{u_{\theta\theta}}{r^2}.
	\]
\end{ej}
\begin{obs}
	Si $\varphi$ és holomorfa en un disc $B_1(0)$, aleshores la podem expressar
	com a sèrie de potències en aquest domini:
	\[
		\varphi(z) = \sum_{k = 0}^{\infty} a_k z^k,
	\]
	amb $a_k = \frac{\varphi^{(k)}(0)}{k!}$. Per tant, en coordenades polars:
	\[
		\varphi(z) = \sum_{k = 0}^{\infty} a_k r^k (\cos(k \theta) + i \sin (k \theta)).
	\]
	Si $a_k = b_k + i c_k$, amb $b_k, c_k \in \real$, podem observar que la part real de $\varphi$,
	\[
		u(z) = \sum_{k = 0}^{\infty} \left( b_k r^k \cos(k \theta) - c_k r^k \sin(k \theta) \right),
	\]
	està expressada en sèrie de Fourier. Així, observem una connexió entre les sèries de Taylor en polars i les sèries de Fourier.
\end{obs}
Finalment, donem exemples de funcions holomorfes que no són polinomis:
\begin{gather*}
	\varphi(z) = e^z = e^x(\cos y + i \sin y) \\
	\varphi(z) = \Log z = \log r + i \theta.
\end{gather*}
D'aquí obtenim altres exemples de funcions harmòniques, com $e^x \cos y$ i $e^x \sin y$
en el cas de l'exponencial i $\log(\sqrt{x^2 + y^2})$ i $\arctan(\frac{y}{x})$ pel logaritme
complex (aquest últim només en un domini simplement connex que no contingui el 0).

\section{Existència i unicitat de solucions}
En aquesta secció, estudiarem els resultats relacionats amb l'existència i unicitat
de solucions de l'equació de Poisson. Mentre que els resultats d'existència seran més
complicats d'obtenir en general, la unicitat la podem demostrar amb relativa facilitat
per a dominis generals de $\real^n$.
\begin{prop}
	Donats $\Omega \subset \real^n$ obert regular ($\C^1$) fitat i funcions $f \colon \Omega \to \real$
	i $g \colon \Omega \to \real$, considerem el problema
	\[
		\begin{cases}
			-\Delta u = f & \text{ a } \Omega, \\
			u = g & \text{ a } \partial\Omega.
		\end{cases}
	\]
	Si el problema admet una solució $u \in \C^2(\overline{\Omega})$, aleshores és única.
\end{prop}
\begin{proof}
	Utilitzarem el mètode integral o d'energia. En primer lloc, suposem que $u,v \in \C^2(\overline{\Omega})$
	són solucions del problema. Aleshores, $w = u-v$ satisfà el problema completament homogeni:
	\[
		\begin{cases}
			-\Delta w = 0 & \text{ a } \Omega, \\
			w = 0 & \text{ a } \partial\Omega.
		\end{cases}
	\]
	Aleshores,
	\[
		0 = \int_{\Omega} w \Delta w = \int_{\partial \Omega} w \frac{\partial w}{\partial \nu} - 
		\int_{\Omega} |\nabla w|^2,
	\]
	on a l'última igualtat hem integrat per parts. Com que $|\nabla w|^2$ és sempre positiu, deduïm:
	\[
		\nabla w = 0 \text{ a } \overline{\Omega} \implies
		\begin{rcases}
			w \equiv \text{constant} \\
			w|_{\partial \Omega} = 0
		\end{rcases}
		\implies w \equiv 0 \text{ a } \overline{\Omega}.
	\]
\end{proof}
\begin{obs}
	A $\real^2$, podem fer una associació entre funcions harmòniques al pla real
	i funcions holomorfes a $\mathbb{C}$. D'aquí es dedueix que tota funció harmònica
	($u \in \C^2(\Omega)$) resulta ser $\C^{\infty}(\Omega)$ i analítica. El mateix
	resultat és cert per a funcions harmòniques en oberts de $\real^n$, però no ho demostrarem aquí.
\end{obs}
Mirant enrere, recordem que per l'equació del calor també es dona aquest efecte regularitzant,
però que això no té perquè ser cert per l'equació d'ones (per exemple, una solució podria ser
$u(x,y) = |x-y|^{2.5}$). Aquesta diferència és deguda a que l'equació de Poisson és e\lgem íptica,
l'equació del calor és parabòlica i l'equació d'ones és hiperbòlica. Tot seguit detallem
una mica més aquesta classficació.

Considerem una EDP de segon ordre a coeficients constants
\[
	A u_{xx} + 2B u_{xy} + C u_{yy} = 0.
\]
Només considerem els termes d'ordre dos, que són els termes dominants. Podem reescriure l'EDP com:
\[
	\tr \left[ 
		\begin{pmatrix}
		  u_{xx} & u_{xy} \\
		  u_{xy} & u_{yy}
		\end{pmatrix}
		\begin{pmatrix}
		  A & B \\
		  B & C
		\end{pmatrix}
	 \right]
	 = \tr \left[ (D^2 u) M \right] = 0.
\]
Ens agradaria trobar una base ortonormal per la matriu dels coeficients (que existirà
perquè és simètrica), de manera que
\[
	0 = \tr ((D^2u) M) = \tr ((D^2 u) O \tilde{D} O^t) = \tr(O^t (D^2 u) O \tilde{D}).
\]
Això és el mateix que fer un canvi de variables entre $(x,y)$ i uns certs $(\xi,\eta)$ a determinar:
\[
	u(x,y) \to \tilde{u} (\xi,\eta) = u\left(C 
	\begin{pmatrix}
		\xi \\
		\eta
	\end{pmatrix}
	\right) \implies D^2 \tilde{u} = C^t (D^2 u) C \text{ o } C(D^2 u)C^t,
\]
on a l'última igualtat deixem com a exercici pel lector determinar quina de les dues
expressions és la correcta. Si prenem $C = O^t$ o $C = O$ (segons la resposta correcta),
l'EDP s'expressa com a
\[
	\tr((D^2\tilde{u})\tilde{D}) = \lambda_1 u_{\xi \xi} + \lambda_2 u_{\eta \eta}.
\]
Reescalant $\xi$ i $\eta$ adequadament, ens queden tres opcions, que és la classificació
d'equacions de 2n ordre lineals al pla $\real^2$:
\begin{itemize}
	\item Si $\lambda_1$ i $\lambda_2$ tenen el mateix signe, ens queda $u_{xx} + u_{yy} = 0$
	i diem que l'equació és e\lgem íptica.
	\item Si $\lambda_1$ i $\lambda_2$ tenen el signe diferent, ens queda $u_{xx} - u_{yy} = 0$
	i diem que l'equació és hiperbòlica.
	\item Si $\lambda_1 = 0$ o $\lambda_2 = 0$,  ens queda $u_{xx} = 0$.
\end{itemize}

Tot seguit, estudiarem l'existència de solucions per l'equació de Laplace en un disc
de $\real^2$. Al problema 3 de la llista 6 de problemes del curs, es demostra que, per l'EDO:
\[
	\begin{cases}
		-u'' = f, \\
		u(0) = u(L) = 0,
	\end{cases}
\]
hi ha existència i unicitat de solucions, i que la solució es pot expressar de la forma
\[
	u(x) = \int_{0}^{L} G(x,y) f(y) \dif y,
\]
on la funció $G(x,y)$ és explícita i rep el nom de funció de Green. La utilitat de $G$
és que si veiem que és $\C^{\infty}$, aleshores les solucions del problema seran $\C^{\infty}$.
\begin{prop}
	Sigui $B_R \subset \real^2$ la bola de radi $R$ centrada a l'origen. Donada
	$g \colon \partial B_R \to \real$ contínua, el problema
	\[
		\begin{cases}
			\Delta u = 0 & \text{a }B_R, \\
			u = g & \text{a } \partial B_R,
		\end{cases}
	\]
	admet una solució $u \in \C^2(B_R) \cap \C^0 (\overline{B_R})$ i ve donada per
	\[
		u(x) = \int_{\partial B_R} P_R (x,y) g(y) \dif y = 
		\frac{R^2-|x|^2}{2 \pi R} \int_{\partial B_R} \frac{g(y)}{|x-y|^2} \dif y.
	\]
\end{prop}
\begin{proof}
	Aplicarem el mètode de separació de variables en coordenades polars. Busquem una solució de la forma
	\[
		u(x) = v(r) w(\theta).
	\]
	Usant l'expressió del laplacià en polars:
	\[
		0 = \Delta u = u_{rr} + \frac{u_r}{r} + \frac{1}{r^2} u_{\theta \theta}
		\implies 0 = v'' w + \frac{v'w}{r} + \frac{vw''}{r^2}.
	\]
	Separant la part que depèn de $r$ i de $\theta$:
	\[
		r^2 \frac{v'' + \frac{v'}{r}}{v} = \frac{-w''}{w} = \lambda,
	\]
	sent $\lambda$ una constant. Com que $w$ és $2\pi$-periòdica, deduïm que:
	\[
		w(x) = a \cos (k \theta) + b \sin (k \theta),
	\]
	amb $\lambda = k^2$, on $k \in \mathbb{Z}$. Pel que fa a l'altra equació:
	\[
		v'' + \frac{v'}{r} - \frac{k^2}{r^2} v = 0.
	\]
	Busquem solucions del tipus $v(r) = r^{\alpha}$:
	\[
		\alpha (\alpha - 1) r^{\alpha-2} + \alpha r^{\alpha-2} - k^2 r^{\alpha-2} = 0
		\implies \alpha^2 - k^2 = 0 \implies \alpha = \pm k.
	\]
	Així, les candidates a solucions són
	\[
		r^k (a \cos(k \theta) + b \sin(k \theta)).
	\]
	D'aquestes, descartem les que tinguin $k$ negatiu, ja que són singulars a l'origen.
	Per tant, la solució general de l'equació seran les combinacions lineals de les candidates:
	\[
		u = u(r, \theta) = \frac{a_0}{2} + \sum_{k = 1}^{\infty} \frac{r^k}{R^k}
		(a_k \cos(k\theta) + b_k \sin(k \theta)).
	\]
	Falta imposar la condició inicial. Com que $g \in \C^0 (\partial B_R)$, la podem expressar
	en sèrie de Fourier:
	\[
		u|_{r = R} = g(\theta) = \frac{a_0}{2} + \sum_{k = 1}^{\infty} (a_k \cos(k\theta) + b_k \sin(k \theta)),
	\]
	amb els coeficients:
	\begin{gather*}
		a_k = \frac{1}{\pi} \int_{0}^{2 \pi} g(\beta) \cos(k\beta) \dif \beta, \\
		b_k = \frac{1}{\pi} \int_{0}^{2 \pi} g(\beta) \sin(k\beta) \dif \beta.
	\end{gather*}
	Així, en l'expressió de la $u$, podem canviar sumatori i integrals:
	\[
		u(r,\theta) = \frac{1}{\pi}\int_{0}^{2\pi} \dif \beta g(\beta) \left( \frac{1}{2} +
		\sum_{k=1}^{\infty} \left( \frac{r^k}{R^k} (\cos(k \beta)\cos(k \theta) +
		\sin(k \beta) \sin(k \theta)) \right)\right).
	\]
	Com que 
	\[(\cos(k \beta)\cos(k \theta) + \sin(k \beta) \sin(k \theta)) = \cos(k(\theta - \beta))
	= \Re(e^{i k (\theta-\beta)}),
	\]
	el sumatori es converteix en
	\begin{gather*}
		u(r, \theta) = \frac{1}{\pi} \int_{0}^{2\pi} \dif \beta g(\beta)\left( \frac{1}{2} +
		\Re \left( \sum_{k=1}^{\infty} \left(\frac{r}{R}\right)^k e^{i k(\theta- \beta)}\right) \right) = \\
		= \frac{1}{\pi}\int_{0}^{2\pi} \dif \beta g(\beta) \left( \frac{1}{2} + \Re \left( 
		 \frac{\frac{r}{R}e^{i (\theta-\beta)}}{1 - \frac{r}{R}e^{i(\theta-\beta)}}\right) \right).
	\end{gather*}
	Fent les operacions pertinents, l'expressió es converteix en:
	\[
		u(r, \theta) = \frac{1}{\pi} \int_{0}^{2 \pi} \dif \beta g(\beta)
		\frac{R^2-r^2}{R^2 + r^2 - 2Rr\cos(\theta-\beta)},
	\]
	que, desfent el canvi a polars, és l'expressió desitjada.
\end{proof}
\begin{defi}[nucli de Poisson]
	El nucli de Poisson pel disc $B_R \subset \real^2$ és la funció
	\[
		P_R (x,y) = \frac{R^2-|x|^2}{2 \pi R} \frac{1}{|x-y|^2}.
	\]
\end{defi}
\begin{col}
	Si $u \in \C^2(\overline{B_R})$, aleshores
	\[
		u(x) = \int_{\partial B_R} P_R (x,y) g(y) \dif y
	\]
	és l'única solució, i és $\C^{\infty}$ com a funció de $x \in B_R$.
\end{col}
\begin{ej}
	Demostreu l'existència de solucions per a la mateixa equació en un domini rectangular de $\real^2$,
	i trobeu el seu nucli de Poisson (potser escrit com una sèrie). És útil separar la condició inicial
	$g = g_1 + g_2 + g_3 + g_4$, on cada $g_i$ correspon a un costat del rectangle, i resoldre el problema
	suposant que totes menys una són nu\lgem es.
\end{ej}

De fet, demostrar existència i unicitat de solucions per a un disc és suficient per a demostrar-ho
per a un obert de $\real^2$ simplement connex, ja que
\begin{itemize}
	\item Per a tot obert $\Omega \subset \real^2$ simplement connex, existeix
	$\varphi \colon \Omega \to D_1$ holomorfa i bijectiva (Teorema de l'aplicació
	conforme de Riemann).
	\item Si $u \colon \Omega \to \real$ és harmònica, aleshores $u \circ \varphi^{-1}$ també ho és.
\end{itemize}
Per tant, en un obert simplement connex de $\real^2$ hi ha existència i unicitat de solucions pel
problema de Laplace.

\section{Probabilitats i el laplacià}
Considerem el següent problema: Tenim una regió fitada $\Omega \subset \real^2$,
de manera que la seva vora està partida en dos subconjunts: $\partial \Omega = 
\Gamma_o \cup \Gamma_t$, $\Gamma_o \cap \Gamma_t = \emptyset$ (la part ``oberta'' i 
la part ``tancada''). Fixem una partícula $x = (x_1,x_2) \in \Omega$, i caminem 
``aleatòriament'' dins de $\Omega$ sortint inicialment de $x$, de manera que:
\begin{itemize}
	\item No privilegio cap direcció.
	\item No tinc memòria.
\end{itemize}
Aquest és un procés de difusió o moviment Brownià. Volem calcular la funció $u(x)$,
que és la probabilitat que el primer cop que el camí toqui la vora $\partial \Omega$,
ho faci a la part oberta $\Gamma_o$.

Discretitzem el problema. Suposem que cada vegada que ens movem, ho fem en passos de distància
$h > 0$, i que ho fem en alguna de les quatre direccions cardinals (nord, est, sud o oest) amb
probabilitat uniforme. Aleshores, per la fórmula de les probabilitats condicionades:
\[
	u^h(x) = \frac{1}{4} (u^h(E) + u^h(O) + u^h(N) + u^h(S)),
\]
o, en coordenades:
\[
	u^h(x_1, x_2) = \frac{1}{4} (u^h(x_1+h,x_2) + u^h(x_1-h,x_2) + u^h(x_1,x_2+h) + u^h(x_1,x_2-h)).
\]
Això és equivalent a:
\[
	0 = [u^h(x_1+h,x_2) + u^h(x_1-h,x_2) - 2u^h(x_1, x_2)] + [u^h(x_1,x_2+h) + u^h(x_1,x_2-h) - 2u^h(x_1, x_2)]
\]
que, dividint per $h^2$, és el quocient incremental per la segona derivada. Així, si $u \in \C^2$,
es té
\[
	0 = \left(u_{x_1 x_1} + u_{x_2 x_2} \right) (x_1, x_2) = \Delta u (x).
\]
Per tant, la probabilitat d'escapar (o el guany esperat) és una funció harmònica. Notem que
hem utilitzat que $u^h \to u$ quan $h \to 0$. Observem que si fóssim a $\real^n$, podríem
resoldre el problema exactament igual.

Considerem un altre problema: Sigui $t > 0$ un instant de temps donat, i $x \in \real$ la
posició d'una partícula unidimensional que es mou aleatòriament per la recta real. Quina
és la probabilitat que després d'un cert temps, la partícula estigui en un cert interval?

Com abans, discretitzem: considerem uns passos de temps $\Delta t = \tau > 0$ i d'espai
$\Delta x = h > 0$, de manera que una partícula a la posició $x$ es pot moure 
a les posicions $x\pm h$ indistintament en un temps $\tau$. La probabilitat
$u(x,t)$ que la partícula estigui a $x$ a temps $t$ és:
\[
	u(x,t) = \frac{1}{2} (u(x-h,t-\tau) + u(x+h,t-\tau)).
\]
Això és equivalent a:
\[
	\frac{u(x,t) - u(x,t-\tau)}{h^2} = \frac{1}{2} \frac{u(x-h,t-\tau) + u(x+h,t-\tau)
	- 2u(x,t-\tau)}{h^2}.
\]
El terme de la dreta, quan $h,\tau \to 0$, és $\frac{1}{2}u_{xx}(x,t)$. El terme de l'esquerra
és una mica més delicat: el numerador només depèn de $\tau$, mentre que el numerador
només depèn de $h$. Per evitar problemes amb els límits, fixem una constant $D > 0$
i fem l'elecció $h^2 = 2D \tau$. D'aquesta manera:
\[
	\frac{u(x,t) - u(x,t-\tau)}{2D \tau} \to \frac{1}{2D} u_t (x,t),
\]
i així l'equació queda
\[
	u_t = D u_{xx},
\]
que és l'equació de difusió. Pel que fa a la condició inicial, sabem que en
temps 0 la partícula està a la posició $x = 0$. Com que $u$ és una funció de densitat de probabilitat,
això es tradueix com $u(x,0) = \delta(x)$, on $\delta$ és la delta de Dirac, que satisfà:
\[
	\int_{-\infty}^{\infty} \delta(x) \dif x = 1, \; \int_{-\infty}^{\infty} f(x) \delta(x) \dif x = f(0).
\]
Podem pensar la delta de Dirac com una funció que és zero arreu excepte a l'origen, on és infinit, i que
imposem que tingui integral 1 per tal que sigui una distribució de probabilitat.

Procedim ara a resoldre l'equació del calor que hem obtingut. Observem que les solucions de l'equació
són invariants pel canvi
\begin{gather*}
	x \to \lambda x, \\ 
	t \to \lambda^2 t,
\end{gather*}
si $\lambda > 0$ és una constant. És a dir, que si $u$ és solució, aleshores $\tilde{u} = 
u(\lambda x, \lambda^2 t)$ també ho és. Ens preguntem si hi ha solucions invariants
pel canvi anterior, és a dir, tals que $\tilde{u} = u$ per a tot $\lambda > 0$. En aquest cas,
\[
	\lambda = \frac{1}{\sqrt{t}} \implies u(x,t) = u(\frac{x}{\sqrt{t}},1) = \phi(\frac{x}{\sqrt{t}}),
\]
que és una funció d'una variable. Es pot comprovar que existeixen solucions de l'equació de difusió del tipus
$u(x,t) = \phi(\frac{x}{\sqrt{t}})$, però no són les que ens interessen, ja que:
\[
	\int_{\real} u(x,t) \dif x = \int_{\real} \phi(\frac{x}{\sqrt{t}}) \dif x =
	\sqrt{t} \int_{\real} \phi(y) \dif y,
\]
que no és constant en el temps! A la vista del resultat, assagem ara solucions del tipus:
\[
	u(x,t) = \frac{1}{\sqrt{t}} \phi(\frac{x}{\sqrt{t}}).
\]
Aquestes solucions sí que tindran integral constant en el temps. Trobem-les:
\begin{gather*}
	u_t = -\frac{x}{2t^2} \phi'(\frac{x}{\sqrt{t}}) - \frac{1}{2t^{3/2}} \phi(\frac{x}{\sqrt{t}}), \\
	u_{xx} = \frac{1}{t^{3/2}} \phi''(\frac{x}{\sqrt{t}}).
\end{gather*}
Imposant l'equació:
\[
	\frac{x}{2t^2} \phi'(\frac{x}{\sqrt{t}}) + \frac{1}{2t^{3/2}} \phi(\frac{x}{\sqrt{t}}) +
	\frac{1}{t^{3/2}} \phi''(\frac{x}{\sqrt{t}}) = 0.
\]
Multiplicant per $2t^{3/2}$:
\[
	\frac{x}{\sqrt{t}} \phi'(\frac{x}{\sqrt{t}}) + \phi(\frac{x}{\sqrt{t}}) + 
	2 \phi''(\frac{x}{\sqrt{t}}) = 0.
\]
Així, fent el canvi $z = \frac{x}{\sqrt{t}}$, ens queda l'EDO:
\[
	2 \phi'' + z \phi' + \phi = 0 \iff (z \phi)' = -2 \phi''.
\]
Integrant,
\[
	z \phi = -2 \phi' + C.
\]
Intuïtivament, veiem que $u$ ha de ser parella en $x$, ja que la partícula té la mateixa probabilitat
d'anar a la dreta i a l'esquerra. En aquest cas, $u'(0,t) = 0 \implies \phi'(0) = 0$, i per tant:
\[
	C = z \phi + 2 \phi' \big|_{z = 0}  = 0.
\]
Observem també que
\[
	(\log \phi)' = \frac{\phi'}{\phi} = -\frac{z}{2} \implies \log \phi = 
	- \frac{z^2}{4} + D \implies \phi = a e^{-z^2/4},
\]
sent $a > 0$ una constant positiva, que trobem imposant que $u(x,t)$ sigui 
una funció de densitat de probabilitat:
\[
	1 = \int_{\real} u(x,t) \dif x = \frac{a}{\sqrt{t}} \int_{- \infty}^{\infty} e^{-x^2/4t} \dif x.
\]
Com que la integral és invariant en el temps, podem prendre qualsevol temps, com ara $t = 1$. 
Veiem que ens queda una integral Gaussiana, que ja sabem calcular:
\[
	\int_{\real} e^{-x^2/4} \dif x = 2 \sqrt{\pi} \implies a = \frac{1}{2 \sqrt{\pi}}
	\implies u(x,t) = \frac{1}{2 \sqrt{\pi t}} e^{-x^2/4t}.
\]
Així, el resultat final és una funció de densitat Gaussiana.

Si en comptes d'una delta de Dirac, la condició inicial fos una funció $g$ general,
aleshores per a cada punt $y \in \real$ podem aproximar la funció com a $g(y)h\delta(y)$
per a cert $h$. Per tant,
\[
	g \approx \sum_{k \in \mathbb{Z}} g(kh)  h \delta(kh) \to
	\sum_{k \in \mathbb{Z}} g(kh)  h \frac{1}{\sqrt{4 \pi t}} e^{\frac{-(x-y)^2}{4t}}
	= \tilde{u}(x,t).
\]
Observem que aquest últim terme és una suma de Riemann, que quan $h \to 0$, esdevé:
\[
	u(x,t) = \int_{R} g(y) \frac{1}{\sqrt{4 \pi t}} e^{\frac{-(x-y)^2}{4t}} \dif y
	= g \ast \left(\frac{1}{\sqrt{4 \pi t}} e^{\frac{-x^2}{4t}} \right).
\]

Anàlogament, podem considerar el problema en dimensions superiors $u_t = D \Delta u$.
Assagem una solució de la forma:
\[
	\Gamma(x,t) = \frac{1}{(Dt)^{n/2}} \varphi(\frac{|x|}{(Dt)^{1/2}}).
\]
\begin{ej}
	Comproveu que
	\[
		\partial_{x_i} r = \partial_r x_i = \frac{x_i}{r}.
	\]
	Proveu també que si $u = u(r)$ és radialment simètrica a $\real^n$, llavors:
	\[
		\Delta u = r^{1-n}(r^{n-1} u_r)_r = u_{rr} + \frac{n-1}{r} u_r.
	\]
\end{ej}
Si se substitueix $\Gamma$ a l'equació (usant l'exercici anterior) i es resol
l'EDO que en resulta, s'obté la solució:
\[
	\Gamma(x,t) = \frac{1}{(4 \pi Dt)^{n/2}} e^{-|x|^2/(4Dt)},
\]
que té la mateixa forma que la unidimensional. També de forma anàloga amb l'anterior,
si la condició inicial és $u(x,0) = g$, aleshores:
\[
	u = \Gamma(\cdot,t) \ast g.
\]

\section{Principi de la mitjana i del màxim}
Tot seguit estudiarem dues propietats molt interessant de les funcions harmòniques
com són la propietat de la mitjana i del màxim, i en veurem les seves conseqüències en
referència al problema de Laplace.
\begin{prop}[Principi de la mitjana]
	Sigui $\Omega \subset \real^n$ un obert, i sigui $u \in \C^2(\Omega) \cap \C^0 (\overline{\Omega})$
	una funció harmònica a $\Omega$. Llavors, per a tot $x_0 \in \Omega$ i per a tota bola
	$\overline{B}_r(x_0) \subset \Omega$, es té:
	\[
		u(x_0) = \fint_{\partial B_r(x_0)} u(y) \dif y = \fint_{B_r(x_0)} u(y) \dif y.
	\]
\end{prop}
\begin{proof}
	Quant a l'integral sobre la vora, veurem que és constant respecte el radi. Per
	a fer-ho, considerem el canvi a polars $r = |x|$, $\sigma = \frac{x}{|x|}$:
	\[
		\frac{d}{dr} \fint_{\partial B_r(x_0)} u(y) \dif y = \frac{d}{dr}
		\fint_{\mathbb{S}^{n-1}} u(x_0 + r \sigma) \dif \sigma = 
		\fint_{\mathbb{S}^{n-1}} \nabla u(x_0 + r \sigma) \cdot \sigma \dif \sigma.
	\]
	Com que per a tot punt $\sigma \in \mathbb{S}^{n-1}$, la normal exterior és $\nu = \sigma$:
	\[
		\fint_{\mathbb{S}^{n-1}} \nabla u(x_0 + r \sigma) \cdot \sigma \dif \sigma
		= \fint_{\partial B_r(x_0)} \frac{\partial u}{\partial \nu} (y) \dif y
		= \frac{1}{|\partial B_r(x_0)|}\int_{B_r(x_0)} \Delta u = 0, 
	\]
	on a la penúltima integral hem usat la identitat de Green (\ref{prop:green}) amb $v = 1$.
	Per tant,
	\[
		\fint_{\partial B_r(x_0)} u \text{ constant en }r \implies
		\fint_{\partial B_r(x_0)} u  = \lim_{r \downarrow 0}  \fint_{\partial B_r(x_0)} u
		= u(x_0).
	\]
	Pel que fa a la integral sòlida, usarem el ``Fubini esfèric'' demostrat al problema
	5 de la llista 4 de problemes:
	\[
		\int_{\Omega} u = \int_{0}^{+ \infty} \dif r \int_{\partial B_r \cap \Omega} \dif \sigma \, u.
	\]
	En el nostre cas, usant Fubini esfèric i el resultat anterior:
	\[
		\fint_{B_r} u = \frac{1}{|B_r|} \int_{B_r} u = \frac{1}{|B_r|} \int_0^r \dif \rho
		\int_{\partial B_{\rho}} u = \frac{1}{|B_r|} \int_0^r \dif \rho |\partial B_{\rho}| u(x_0).
	\]
	Com que
	\[
		|B_r| = \int_{B_r} 1 = \int_0^r \dif \rho \int_{\partial B_{\rho}} 1 =
		 \int_0^r \dif \rho |\partial B_{\rho}|,
	\]
	obtenim:
	\[
		\fint_{B_r} u  = \frac{1}{|B_r|} \int_0^r \dif \rho |\partial B_{\rho}| u(x_0)
		= u(x_0).
	\]
\end{proof}
\begin{obs}
	El recíproc també és cert, en el sentit que si una funció $u \in \C^0(\Omega)$ satisfà la
	propietat de la mitjana, aleshores $u \in \C^2(\Omega)$ i $\Delta u = 0$.
\end{obs}
\begin{prop}[Principi del màxim i del mínim]
	Sigui $\Omega \subset \real^n$ un obert fitat, i sigui $u \in \C^2(\Omega) \cap
	\C^0(\overline{\Omega})$. Aleshores,
	\begin{enumerate}[i)]
		\item Si $-\Delta u \leq 0$ a $\Omega$, llavors la funció $u$ assoleix
		un màxim a $\partial \Omega$.
		\item Si $-\Delta u \geq 0$ a $\Omega$, llavors la funció $u$ assoleix
		un mínim a $\partial \Omega$.
		\item Si $u$ és harmònica, aleshores satisfà el principi del màxim i del mínim.
	\end{enumerate}
\end{prop}
\begin{proof}
	Només cal demostrar $i)$, la resta se segueixen immediatament. En primer lloc,
	suposem que $-\Delta u(x) < 0$ per a tot $x \in \Omega$ (després ens reduïrem a 
	aquest cas). Sigui $x_0 \in \Omega$ un punt de màxim interior. Llavors, $\Delta u(x_0) \leq 0$,
	entrant en contradicció. Per tant, $u$ ha d'assolir un màxim a la vora.
	
	En conseqüència, només ens cal reduir-nos al cas $-\Delta u(x) < 0$. Suposem $-\Delta u(x) \leq 0$
	i, per a cert $\varepsilon > 0$, considerem la funció
	\[
		u_{\varepsilon} (x) = u(x) + \varepsilon x_1^2 \implies \Delta u_{\varepsilon} =
		\Delta u + 2 \varepsilon > 0.
	\]
	En aquest cas, per a tot $x \in \overline{\Omega}$ se satisfà:
	\[
		u(x) \leq u_{\varepsilon}(x) \leq \max_{\overline{\Omega}} u_{\varepsilon} = 
		\max_{\partial \Omega} u_{\varepsilon}.
	\]
	Com que $\Omega$ és fitat, existeix una constant $C$ tal que $x_1^2 \leq C$ a $\Omega$.
	Per tant,
	\[
		\max_{\partial \Omega} u_{\varepsilon} \leq \max_{\partial \Omega} u + \varepsilon C.
	\]
	Per tant, fent $\varepsilon \to 0$, obtenim:
	\[
		u(x) \leq \max_{\partial \Omega} u \implies \max_{\overline{\Omega} }u \leq \max_{\partial \Omega} u.
	\]
	L'altra desigualtat és immediata.
\end{proof}
Notem que la hipòtesi de $\Omega$ fitat és important: $u(x,y) = e^x \sin y$ és harmònica.
Si $\Omega = \real \times (0,\pi)$, aleshores $u$ és nu\lgem a a la vora però $u \neq 0$.
\begin{col}
	El problema de Dirichlet
	\[
		\begin{cases}
			-\Delta u = f(x) & x \in \Omega \\
			u = g(x) & x \in \partial \Omega,
		\end{cases}
	\]
	si té solució, llavors aquesta és única.
\end{col}
\begin{proof}
	Si $u_1$ i $u_2$ són solucions, aleshores $v = u_1 - u_2$ resol el problema completament homogeni.
	Com que $\Delta v = 0$, aleshores $v$ assoleix el màxim i el mínim a la vora. Però a la vora aquesta
	funció és idènticament nu\lgem a, així que el màxim i el mínim han de ser zero, concloent que $v \equiv 0$.
\end{proof}
Per a l'equació de difusió també és certa una versió del principi del màxim, que
estudiem tot seguit. Considerem l'equació de difusió en un obert $\Omega \subset \real^n$,
i considerem el cilindre $Q_T = \Omega \times (0,T)$.
\begin{defi}[funció!subcalòrica]
	Donada $u \in \C^2(\Omega) \cap \C^0(\overline{\Omega})$, direm que $u$ és
	subcalòrica si $u_t - \Delta u \leq 0$ a $Q_T$.
\end{defi}
\begin{defi}[frontera parabòlica]
	La frontera parabòlica de $Q_T$ és
	\[
		\partial_P Q_T = (\overline{\Omega} \times \{ 0\}) \cup (\partial \Omega
		\times [0,T]).
	\]
\end{defi}
És a dir, la frontera parabòlica consta de la tapa inferior i la frontera lateral, però no
la tapa de dalt (que correspondria a $t = T$).
\begin{prop}[Principi del màxim per l'equació de difusió]
	Sigui $\Omega$ un obert fitat de $\real^n$, i sigui $T > 0$. Considerem $u \in
	\C^2(Q_T) \cap \C^0 (\overline{Q_T})$, i $u_t - \Delta u \leq 0$ a $Q_T$. Llavors,
	$u$ assoleix el màxim a la seva frontera parabòlica $\partial_P Q_T$.
\end{prop}
\begin{proof}
	Procedim com en el cas del laplacià. En primer lloc, suposem que $u_t - \Delta u < 0$ a $Q_T$.
	Per contradicció suposem que el màxim s'assoleix a $\overline{Q_T} \setminus \partial_P Q_T$.
	Per tant, hi ha dues opcions:
	\begin{enumerate}[i)]
		\item El màxim s'assoleix a l'interior $Q_T$. En aquest cas, si $(x_0, t_0) \in Q_T$ és
		un punt de màxim:
		\[
			\begin{rcases}
				u_t (x_0,t_0) = 0 \\
				\Delta u(x_0, t_0) \leq 0
			\end{rcases}
			\implies (u_t - \Delta u) (x_0, t_0) \geq 0,
		\]
		arribant a contradicció.
		\item El màxim s'assoleix a $\Omega \times \{ T\}$. En aquest cas,
		\begin{itemize}
			\item $u(x_0, \cdot) \colon [0,T] \to \real$ té un màxim a $T$, de manera que
			$u_t (x_0,T) \geq 0$.
			\item $u(\cdot,T) \colon \Omega \to \real$ té un màxim a $x_0 \in \Omega$, de manera que
			$\Delta u(x_0, T) \leq 0$.
		\end{itemize}
		Combinant-ho, obtenim que $(u_t - \Delta u) (x_0, t_0) \geq 0$, arribant a contradicció.
	\end{enumerate}
	Per tant, és suficient reduir-nos al cas anterior, i això ho podem considerant la funció:
	\[
		u_{\varepsilon} = u + \varepsilon x_1^2.
	\]
	Els detalls són iguals que a la demostració anterior.
\end{proof}
De fet, una versió del principi del màxim més general es pot enunciar per l'equació del calor
amb condicions de Dirichlet:
\[
	\begin{cases}
		u_t - \Delta u = f(x,t) & (x,t) \in Q_T \\
		u(x,t) = d(x,t) & (x,t) \in \partial \Omega \times [0,T] \\
		u(x,0) = g(x) & \Omega \times \{0 \}.
	\end{cases}
\]
Si $f \leq 0$, aleshores el màxim de $u$ serà el màxim entre els màxims de $g$ i el de $d$.

El principi del màxim també és cert per a altres equacions. Per exemple, quan $n=2$,
\[
	Lu = u_{x_1 x_1} + 3 u_{x_2 x_2} - 5 u_{x_2}.
\]
Si se satisfà $Lu \leq 0$ a $\Omega$, aleshores el màxim se satisfà a la vora. La comprovació
d'aquest fet és anàloga a les anteriors i es deixa com a exercici. 
Una versió més general del principi del màxim la podem obtenir considerant:
\[
	Lu = \tr(A(x) D^2 u(x)) + b(x) \cdot \nabla u(x),
\]
amb $A(x)$ una matriu simètrica definida positiva per a tot $x \in \Omega$.
\begin{lema}
	Si $-B = -D^2 u(x_0)$ i $A = A(x_0)$ són matrius definides positives, aleshores
	$\tr(-BA) \geq 0$.
\end{lema}
A partir d'aquest lema d'àlgebra lineal, es pot fer la demostració del principi del màxim igual que abans.

\section{El principi de comparació}
Considerem el problema de Dirichlet:
\[
	\begin{cases}
		-\Delta u = f(x) & x \in \Omega \\
		u = g(x) & x \in \partial \Omega,
	\end{cases}
\]
\begin{defi}[subsolució]
	Direm que $v$ és una subsolució del problema si $v \in \C^2(\Omega) \cap 
	\C^0(\overline{\Omega})$ i
	\[
		\begin{cases}
			-\Delta v \leq f(x) & x \in \Omega \\
			v \leq g(x) & x \in \partial \Omega.
		\end{cases}
	\]
\end{defi}
\begin{defi}[supersolució]
	Direm que $w$ és una supersolució del problema si $w \in \C^2(\Omega) \cap 
	\C^0(\overline{\Omega})$ i
	\[
		\begin{cases}
			-\Delta w \geq f(x) & x \in \Omega \\
			w \geq g(x) & x \in \partial \Omega.
		\end{cases}
	\]
\end{defi}
\begin{prop}[Principi de comparació]
	Considerem el problema de Dirichlet a un obert fitat $\Omega \subset \real^n$.
	Si $v$ i $w$ són subsolució i supersolució respectivament, llavors $v \leq w$ 
	a $\overline{\Omega}$. Si, a més, $u$ és solució, llavors $v \leq u \leq w$.
\end{prop}
\begin{proof}
	És conseqüència del principi del màxim. Considerem $\varphi = v - w$,
	subsolució del problema completament homogeni. Llavors,
	\[
		\varphi(x) \leq \max_{\partial \Omega} u \leq 0.
	\]
\end{proof}

Tot seguit, veurem dues aplicacions del principi de comparació. Considerem en primer
lloc l'equació
\[
	\begin{cases}
		- \Delta u = 1 & \text{ a } \Omega \\
		u = 0 & \text{ a } \partial \Omega,
	\end{cases}
\]
sent $\Omega \in \real^n$ un obert fitat. Pel principi del màxim, $u \geq 0$ a $\Omega$.
Ara voldríem trobar una fita superior per a $u$, i ho farem trobant una supersolució del problema.
Suposem que $\Omega \subset B_R$ per a alguna $R$. Resolem:
\[
	\begin{cases}
		- \Delta v = 1 & \text{ a } B_R \\
		v = 0 & \text{ a } \partial B_R,
	\end{cases}
\]
Per inspecció, podem suposar que $v$ tindrà simetria radial. Per tant, considerant el laplacià
en polars, només cal integrar:
\[
	-r^{1-n}(r^{n-1} v_r)_r = 1.
\]
Es pot comprovar que la solució obtinguda així és:
\[
	v(x) = \frac{1}{2n} (R^2 - |x|^2).
\]
Com que $v \geq 0$ a $\overline{B_R}$, obtenim que $v$ és supersolució del problema a $\Omega$.
Per tant,
\[
	0 \leq u(x) \leq v = \frac{R^2 - |x|^2}{2n} \leq \frac{R^2}{2n}.
\]

Una altra aplicació del principi de comparació la veiem en la següent equació de difusió:
\[
	\begin{cases}
		u_t - \Delta u = 0 & \text{ a } \Omega \times (0,+\infty) \\
		u = 0 & \text{ a } \partial \Omega \times (0,+\infty) \\
		u(x,0) = g(x) & \text{ a } \overline{\Omega}.
	\end{cases}
\]
\begin{prop}
	En aquest problema, existeix una constant $C$ depenent de $\Omega$ tal que,
	si $g \in \C^0(\overline{\Omega})$, llavors:
	\[
		u(x,t) \leq \frac{C}{t^{n/2}} ||g||_{L^{\infty}}.
	\]
\end{prop}
\begin{proof}
	Ja vam veure que una solució de l'equació de difusió era:
	\[
		v(x,t) = \Gamma(x,t) = \frac{1}{(4 \pi t)^{n/2}} e^{\frac{-|x|^2}{4t}}.
	\]
	Com que no està definida per $t=0$, la podem desplaçar temporalment per a que ho estigui.
	També podem reescalar-la per una certa constant, obtenint:
	\[
		\tilde{v}(x,t) = \frac{C_0 ||g||_{L^{\infty}}}{(4 \pi (t+1))^{n/2}}
		e^{\frac{|x|^2}{4(t+1)}}.
	\]
	Volem escollir $C_0$ per tal que la funció compleixi:
	\[
		\begin{cases}
			\tilde{v}_t - \Delta \tilde{v} = 0 & \text{ a } \Omega \times [0,+\infty) \\
			\tilde{v} \geq 0 & \text{ a } \partial \Omega \times [0,+\infty) \\
			\tilde{v} \geq g & \text{ a } \Omega \times \{ 0\} \\
			\tilde{v} \in \C^0 (\overline{\Omega} \times [0, +\infty)).
		\end{cases}
		%]
	\]
	Per a $t = 0$, se satisfà:
	\[
		\tilde{v}(x,0) = \frac{C_0 ||g||_{L^{\infty}}}{(4 \pi)^{n/2}} e^{\frac{-|x|^2}{4}} \geq
		\frac{C_0}{(4\pi)^{n/2}} e^{\frac{-R^2}{4}} ||g||_{L^{\infty}}.
	\]
	Si prenem $C_0 = (4 \pi)^{n/2} e^{R^2/4}$, aleshores:
	\[
		\tilde{v}(x,0) \geq ||g||_{L^{\infty}} \geq g(x).
	\]
	Pel principi de comparació, sabem que $u(x,t) \leq \tilde{v} (x,t)$, i per tant:
	\[
		u(x,t) \leq \frac{e^{\frac{R^2}{4}}}{(t+1)^{n/2}} e^{\frac{-|x|^2}{4(t+1)}}
		||g||_{L^{\infty}} \leq \frac{e^{\frac{R^2}{4}}}{t^{n/2}} ||g||_{L^{\infty}}.
	\]
\end{proof}
