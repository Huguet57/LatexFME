\chapter{Aplicacions lineals}
\section{Definició i tipus}
\begin{defi}[aplicació lineal]
	Una aplicació $f$ entre dos $\k$-e.v. $E$ i $F$,
	\begin{equation*}
	\begin{aligned}
		f \qcolon & E &&\to &&F\\
				 & v &&\mapsto &&f(v)\,,
	\end{aligned}
	\end{equation*}
	és una aplicació lineal (\textit{linear map} o \textit{linear 
	transformation}) si i només si
	\begin{gather*}
		\forall u, v \in E\quad f(u + v) = f(u) + f(v)\,,\\
		\forall c\in\k\ \forall v \in E \quad f(c\cdot v) = c\cdot f(v)\,.
	\end{gather*}
\end{defi}

\begin{defi}[aplicació identitat]
	L'aplicació lineal entre un $\k$-e.v. $E$ i sí mateix que relaciona cada 
	$v\in E$ amb sí mateix, és a dir,
	\begin{equation*}
	\begin{aligned}
		\Id \qcolon & E &&\to &&E\\
				    & v &&\mapsto &&v \quad \forall v\in E\,,
	\end{aligned}
	\end{equation*}
	s'anomena aplicació identitat i es denota amb $\Id$.
\end{defi}

\begin{defi}[homotècia]
	Sigui $E$ un $\k$-e.v., i sigui $\lambda \in \k$ un escalar. Tota 
	aplicació lineal de la forma
	\begin{equation*}
	\begin{aligned}
		f \qcolon & E &&\to &&E\\
		& v &&\mapsto &&\lambda v \quad \forall v\in E
	\end{aligned}
	\end{equation*}
	s'anomena \textit{homotècia}.
\end{defi}

\begin{prop}
	Si una aplicació $f$ entre dos $\k$-e.v. $E$ i $F$ és lineal, 
	necessàriament $f(0_E) = 0_F$, on $0_E$ i $0_F$ són, respectivament, el 
	vector nul d'$E$ i de $F$.
	\begin{proof}
		Sigui $v$ un vector d'$E$ tal que $f(v) = u \in F$. Per la linearitat 
		de $f$, 
		$f(0_E) = f(0\cdot v) = 0 \cdot f(v) = 0\cdot u = 0_F$\,.
	\end{proof}
\end{prop}

\begin{prop}
	Siguin $E$ i $F$ dos $\k$-e.v.; sigui $\{u_1,\ldots,u_n\}$ una base d'$E$ i 
	siguin $v_1,\ldots,v_n\in F$ vectors qualssevol de $F$. Llavors existeix 
	una única aplicació lineal $f$ tal que
	\[
		f(u_i) = v_i\quad \forall i \in \{1,\ldots,n\}\,.
	\]
	És a dir, que la imatge d'una base determina unívocament una aplicació 
	lineal.
	\begin{proof}
		Suposem que existeixen dues aplicacions lineals, $f\colon E \to F$ i 
		$g\colon E \to F$, tals que
		\begin{gather*}
			f(u_i) = v_i\quad \forall i \in \{1,\ldots,n\}\,,\\
			g(u_i) = v_i\quad \forall i \in \{1,\ldots,n\}\,.
		\end{gather*}
		Per cada vector $w\in E$, existeixen uns únics coeficients 
		$\lambda_1,\ldots, \lambda_n\in\k$ tals que
		\[
			w = \sum_{i=1}^n \lambda_i u_i\,,
		\]
		ja que els vectors $u_1,\ldots,u_n$ formen una base (i per tant les 
		coordenades de $w$ respecte aquesta \hyperref[teo:unique-coord]{són 
		úniques}).
	
		Llavors, emprant la linearitat de $f$,
		\[
			f(w) = f(\lambda_1 u_1 + \cdots + \lambda_n u_n) = \lambda_1 f(u_1) 
			+ \cdots + \lambda_n f(u_n) = \sum_{i=1}^{n} \lambda_i v_i\,.
		\]
		D'altra banda, $g$ també és lineal, i per tant
		\[
			g(w) = g(\lambda_1 u_1 + \cdots + \lambda_n u_n) = \lambda_1 g(u_1) 
			+ \cdots + \lambda_n g(u_n) = \sum_{i=1}^{n} \lambda_i v_i\,.
		\]
		Com que els coeficients $\lambda_1,\ldots,\lambda_n$ són únics, 
		concloem que $f(w) = g(w)$. Però $w$ és un vector arbitrari, ergo
		\[
			\forall w \in E\quad f(w) = g(w) \Leftrightarrow f \equiv g\,.
		\]
	\end{proof}
\end{prop}

\begin{defi}[monomorfisme]
	Direm que una aplicació lineal $f$ és un \textit{monomorfisme} si i només 
	si és injectiva.
\end{defi}

\begin{defi}[epimorfisme]
	Direm que una aplicació lineal $f$ és un \textit{epimorfisme} si i només si 
	és suprajectiva (exhaustiva).
\end{defi}

\begin{defi}[isomorfisme]
	Direm que una aplicació lineal $f$ és un \textit{isomorfisme} si i només si 
	és un monomorfisme i un epimorfisme alhora---és a dir, si és bijectiva.
\end{defi}

\begin{defi}[endomorfisme]
	Direm que una aplicació lineal $f$ és un \textit{endomorfisme} si i només 
	si és de la forma $f\colon E \to E$---és a dir, si l'espai d'arribada i 
	l'espai de sortida coincideixen.
\end{defi}

\begin{defi}[automorfisme]
	Direm que una aplicació lineal $f$ és un \textit{automorfisme} si i només 
	si és un endomorfisme i un isomorfisme alhora---és a dir, si és un 
	endomorfisme bijectiu.
\end{defi}

\section{Nucli i imatge}
\begin{defi}[imatge d'una aplicació]
	Sigui $f\colon E \to F$ una aplicació lineal. La imatge de $f$ es defineix 
	com
	\[
		\im f \defeq \{v \in F \mid \exists u \in E \colon f(u) = v \}\,;
	\]
	és a dir, com el conjunt de tot vector de $F$ que és la imatge d'[almenys 
	]un vector d'$E$.
\end{defi}


\begin{defi}[nucli]
	Sigui $f\colon E \to F$ una aplicació lineal. El nucli de $f$ es defineix 
	com
	\[
		\nuc f \defeq \{u\in E \mid f(u) = 0_F \}\,;
	\]
	és a dir, el conjunt de vectors d'$E$ que es transformen en el vector nul 
	de $F$ quan se'ls hi aplica $f$.
\end{defi}

\begin{prop}
	Una aplicació lineal $f\colon E \to F$ és suprajectiva (exhaustiva) si i 
	només si $\im f = F$.
	\begin{proof}
		D'una banda, per definició, $\im f \subseteq F$, sigui quina sigui 
		l'aplicació $f$. A més, si $f$ és suprajectiva, per tot $v\in F$ 
		existeix un $u\in E$ tal que $f(u) = w$. Per tant, per la definició 
		d'imatge, tot $v \in F$ pertany a $\im f\,$; ergo $F \subseteq \im f$. 
		Per tant $\im f = F$.
		
		La implicació recíproca és directa: si $\im f = F$, per definició 
		d'imatge, tot vector $v\in F$ compleix $\exists u \in E \colon f(u) = 
		v$. Aquesta és la definició de que $f$ sigui suprajectiva.
	\end{proof}
\end{prop}

\begin{prop}
Una aplicació lineal $f\colon E \to F$ és injectiva si i 
només si $\nuc f = \{0_E\}$.

\begin{proof}
	Suposem que $f$ és injectiva i que $\exists w \in \nuc f\colon w \ne 0_E$. 
	Llavors, per tot vector $u\in E$,
	\[
		u + w \ne u \quad\land\quad f(u + w) = f(u) + f(w) = f(u) + 0_F = 
		f(u)\,,
	\]
	cosa que contradiu la definició d'injectivitat (és a dir,  $f(a) = f(b) 
	\rightarrow a = b$). Per tant, si $f$ és injectiva, $\nuc f = \{0_E\}$.
	
	D'altra banda, suposem que $\nuc f = \{0_E\}$ però que $f$ no és injectiva.
	Llavors, existeixen vectors $u,v\in E$ tals que 
	\[
		u \ne v \quad \land \quad f(u) = f(v) \sRarr	f(u) - f(v) = f(u-v) = 
		0_F \,.
	\]
	Com que per hipòtesi $\nuc f = \{0_E\}$, $u-v$ ha de ser per força $0_E$, i 
	per tant $u - v = 0_E \sRarr u = v$, cosa que contradiu la hipòtesi que 
	$u\ne v$. Per tant queda demostrat el recíproc.
\end{proof}
\end{prop}


\begin{prop}
	El nucli d'una aplicació lineal $f\colon E\to F$ és un subespai vectorial 
	d'$E$.
	\begin{proof}
		Per tot parell de vectors $u,v\in \nuc f$,
		\[
			f(u + v) = f(u) + f(v) = 0_F + 0_F = 0_F \sRarr (u+v)\in \nuc f\,.
		\]
		Per tot escalar $c \in \k$ i vector $v \in \nuc f$,
		\[
			f(c\cdot v) = c\cdot f(v) = c\cdot 0_F = 0_F \sRarr c\cdot v \in 
			\nuc f\,.
		\]
	\end{proof}
\end{prop}

\begin{defi}[imatge d'un subconjunt]
	Donada una aplicació lineal $f\colon E \to F$, la imatge d'un subconjunt 
	$W\subseteq E$ es defineix com
	\[
		f(W) \defeq \{v \in F\mid \exists u\in W \colon f(u) = v\}\,.
	\]
\end{defi}

\begin{prop}\label{prop:image-sev}
	Donada una aplicació lineal $f\colon E \to F$, si $W$ és un subespai 
	vectorial d'$E$, $f(W)$ és un subespai vectorial de $F$.
	\begin{proof}
		Siguin $u,v \in f(W)$ vectors tals que $u = f(w)$ i $v = f(y)$, on $w,y 
		\in W$ (els vectors $w,y$ existeixen per la definició de $f(W)$). 
		Llavors
		\[
			u + v = f(w) + f(y) = f(w+y)\,.
		\]
		Com que $W$ és un espai vectorial i per tant està tancat per la suma, 
		tenim que $w+y \in W$; ergo
		existeix un vector de $W$ la imatge del qual sigui $(u+v)$; és a dir, 
		$(u+v)\in f(W)$. A més, per tot escalar $c \in \k$,
		\[
			c\cdot u = c\cdot f(w) = f(c\cdot w)\,.
		\]
		Novament, raonem que $W$ és un e.v. i per tant està tancat per la 
		multiplicació escalar: $c\cdot w \in W$. Concloem que existeix un 
		vector de $W$ (ergo, $c\cdot w$) tal que la seva imatge sigui $c\cdot 
		u$; és a dir, $c\cdot u \in f(W)$.
	\end{proof}
\end{prop}

\begin{col}
	La imatge d'una aplicació lineal $f\colon E \to F$ és un subespai vectorial 
	de $F$.
	\begin{proof}
		$E\subseteq E$, i per tant, per la proposició \ref{prop:image-sev}, 
		$f(E) \doteq \im f$ és un s.e.v. de $F$.
	\end{proof}
\end{col}

\begin{defi}[preimatge]
	Donada una aplicació lineal $f\colon E \to F$, definim la preimatge d'un 
	subconjunt $W\in F$ com
	\[
		f^{-1}(W) \defeq \{u \in E \mid f(u) \in W \}\,.
	\]
\end{defi}

\section{Composició}
\begin{defi}[composició d'aplicacions]
	Donades dues aplicacions lineals $f\colon E \to F$ i $g\colon F \to G$ 
	definirem la \textit{composició de g amb f} com l'aplicació lineal
	\begin{equation*}
	\begin{aligned}
		g\circ f \qcolon & E &&\to &&G\\
		& v &&\mapsto &&(g\circ f)(v) \defeq g(f(v))\,.
	\end{aligned}
	\end{equation*}
	Usarem la notació $f_n \circ f_{n-1} \circ \cdots \circ f_1$ per 
	referir-nos a $f_n\circ (f_{n-1} \circ (\cdots \circ(f_2 \circ f_1) )$.
\end{defi}

\begin{defi}[inversa d'una aplicació]
	Direm que una aplicació lineal $f\colon E \to F$ és invertible si existeix 
	una aplicació lineal $g\colon F \to E$ tal que $g \circ f = f \circ g = 
	\Id_E$. En aquest cas direm que $g$ és l'inversa de $f$.
\end{defi}

\begin{prop}
	Una aplicació lineal $f$ és invertible si i només si és un isomorfisme.
\end{prop}