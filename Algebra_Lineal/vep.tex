\chapter{Diagonalització}
%TODO: review

\section{Matrius diagonals}

\begin{defi}[diagonalitza]
	Diem que un endomorfisme $f$ d'un $\k$-espai vectorial $E$ diagonalitza si existeix una base $V$ tal que la matriu de $f$ en base $V$, $M_V$, sigui diagonal. Direm que ``$f$ és diagonalitzable en $\k$'' i ``$f$ és diagonal en base $V$''.
\end{defi}
\begin{obs}
	Sigui $f$ un endomorfisme que diagonalitza en base $V$. Llavors,
	\[
		M_V(f) = A_{e\to V} M_e(f) A_{V\to e} = SDS^{-1}\,,
	\]
	on $S = A_{V\to e}$ és la matriu de canvi de base de $V$ a la base canònica.
\end{obs}

\begin{defi}[matriu diagonalitzable]
	Direm que una matriu $M\in\matspace[\k]{n}$ diagonalitza si existeix una matriu invertible $S\in\matspace[\k]{n}$ tal que
	\[
		D = S^{-1}MS \in \matspace[\k]{n}
	\]
	sigui una matriu diagonal.
\end{defi}

En aquesta darrera definició, la matriu $M$ és equivalent a la matriu d'un endomorfisme $f\colon \real^n \to \real^n$ en una certa base (per exemple la canònica).

\section{Vectors i valors propis}

\begin{defi}[VEP, VAP]
	Sigui $f\in \End (E)$. Direm que un vector $v\in E$ és un \textit{vector propi} de $f$ si $v \ne 0$ i
	\[
		\exists \lambda \in \k \qcolon f(v) = \lambda v\,.
	\]
	Anomenarem l'escalar $\lambda$ corresponent el \textit{valor propi} corresponent a $v$.
\end{defi}

\begin{lema}\label{lema:VAP}
	\begin{enumerate}[i)]
		\item Un vector $u\in E$ és un VEP (de VAP $\lambda \in \k$) de $f$ si i només si $u\in \nuc (f - \lambda\Id)$.
		\item $\lambda \in \k$ és un VAP d'algun VEP de $f$ si i només si $\det (f - \lambda \Id) = 0$.
	\end{enumerate}
	\begin{proof}
		\begin{enumerate}[i)]
			\item[]
			\item $u$ és un VEP $\Leftrightarrow$ $\exists \lambda \in\k\colon f(u) = \lambda u$ $\Leftrightarrow$ $f(u) - \lambda u = (f - \lambda \Id)(u) = 0$ $\Leftrightarrow$ $u \in \nuc (f - \lambda\Id)$
			\item Si $\det (f - \lambda \Id) \ne 0$, la única solució al sistema $(M(f) - \lambda\Id)x = 0$ és la solució trivial $x = 0$, i per tant no hi ha cap vector $x$ no nul tal que $M(f)x = \lambda x$.
		\end{enumerate}
	\end{proof}
\end{lema}

\begin{defi}
	Sigui $\lambda\in\k$ un vector propi de $f$. Definirem l'\textit{espai propi} de $\lambda$ com
	\[
		E_\lambda \defeq\nuc (f - \lambda\Id)\,.
	\]
\end{defi}

\begin{obs}
	Segons el lema \ref{lema:VAP}, tot vector propi de $f$ de VAP $\lambda$ és un element d'$E_\lambda$.
\end{obs}

\begin{defi}
	Definirem l'espectre de $f$ com el conjunt de VAPs de $f$, i ho denotarem per $\sigma(f)$.
\end{defi}

\section{Polinomi característic}

\begin{defi}[polinomi característic d'una matriu]
	Sigui $A\in\matspace[\k]{n}$. Definirem el polinomi característic d'$A$ com
	\[
		P_A(x) \defeq \det (A - x\Id)\,.
	\]
\end{defi}

\begin{prop}
	Siguin $f\in\End(E)$ un endomorfisme. El polinomi característic de la matriu de $f$ en una certa base és independent de la base.
	\begin{proof}
		Sigui $U$ una base d'$E$, i sigui $A = M_U(f)$. Sigui $V$ una altra base d'$E$, i sigui $S$ la matriu de canvi de base de $U$ a $V$. Llavors
		\[
			B \defeq M_V(f) = SAS^{-1}\,.
		\]
		Per tant, el polinomi característic de $B$ serà
		\begin{multline*}
			P_B(x) = \det(B - x\Id) = \det(SAS^{-1} - xSS^{-1}) = \det[(SA - xS)S^{-1}] = \\
			= \det (SA - Sx\Id) \det S^{-1} = \det[S(A - x\Id)]\det(S^{-1}) =\\
			= \det S \det(A - x\Id) \det S^{-1} = \det(A - x\Id) = P_A(x)\,,
		\end{multline*}
		ja que $\det S\det S^{-1} = 1$\,.
	\end{proof}
\end{prop}

\begin{col}
	Sigui $A$ una matriu diagonalitzable, tal que $A = SDS^{-1}$ per alguna matriu diagonal $D$ i matriu invertible $S$. Els valors propis d'$A$ són els elements de la diagonal de $D$.
	\begin{proof}
		\[
			P_A(x) = P_D(x) = \det(D - x\Id) = \prod_{i=1}^{n} (d_{ii} - x)\,,
		\]
		ergo $d_{ii}$ és un VAP per tot $i\in\{1,\ldots,n\}$.
	\end{proof}
\end{col}

\begin{defi}
	Si $f\in\End(E)$ definirem el polinomi característic de $f$ com $P_f(x) \defeq P_M(x)$, on $M$ és la matriu de $f$ (en qualsevol base).
\end{defi}

\begin{prop}
	Sigui $f$ un endomorfisme i
	\[
		P_f(x) = \sum_{i=0}^{n} c_ix^{i}
	\]
	el seu polinomi característic, on $c_0,\ldots, c_n\in\k$ són els coeficients. Llavors
	\begin{gather*}
		c_0 = \det f\\
		c_{n-1} = (-1)^{n-1}\tr f\\
		c_n = (-1)^n\,.
	\end{gather*}
	\begin{proof}
		Sigui $\tilde{A} = A - x\Id$.
		\begin{multline*}
			P_f(x) = \det (A - x\Id) = \sum_{\sigma\in S_n} \sgn (\sigma) \prod_{i=1}^{n} \tilde{a}_{i,\sigma(i)} = \\
			%
			 = \sum_{\sigma\in S_n} \sgn (\sigma)\left(\prod_{i\ne \sigma(i)} a_{i,\sigma(i)} \prod_{i=1}^{n} (a_{i,i} - x)\right) = \sum_{\sigma\in S_n} \left[\prod_{i\ne \sigma(i)} a_{i,\sigma(i)}\left( \prod_{i=1}^{n} a_{i,i} + [x]\right)\right] = \\
			 %
			 = \sum_{\sigma\in S_n} \sgn(\sigma)\prod_{i=1}^n a_{i,\sigma(i)} + \sum_{\sigma\in S_n} \left(\prod_{i\ne \sigma(i)} a_{i,\sigma(i)}\right)[x] = [x] + \det A\,,
		\end{multline*}
		on $[x]$ denota un polinomi sense terme independent. Per tant $c_0 = \det A$.
	\end{proof}
\end{prop}

\begin{col}
	La matriu d'un endomorfisme té el mateix determinant i la mateixa traça en qualsevol base.
\end{col}

\subsection{Multiplicitat algebraica i geomètrica}

\begin{defi}[multiplicitat algebraica]
	Definirem la multiplicitat algebraica d'un VAP $\lambda$ ($a_\lambda$) com la multiplicitat de l'arrel $\lambda$ en el polinomi característic.
\end{defi}

\begin{defi}[multiplicitat geomètrica]
	Donat un endomorfisme en un $\k$-e.v. de dimensió $n$, definirem la multiplicitat geomètrica d'un VAP $\lambda$ com
	\[
		g_\lambda \defeq \dim E_\lambda = n - \rang (M(f) - \lambda\Id)\,.
	\]
\end{defi}

\begin{lema}\label{lema:geom-le-algeb}
	Per tot VAP $\lambda$, 
	\[
		1 \le g_\lambda \le a_\lambda\,. 
	\]
	\begin{proof}
		El fet que $1\le g_\lambda$ és conseqüència directa del fet que $\det(f - \lambda\Id) = 0$ (ergo $\nuc (f - \lambda\Id) \supsetneq \{0\}$, per tant la dimensió del nucli és com a mínim 1).
		
		Ara demostrem que $g_\lambda \le a_\lambda$. Sigui $\B_{\restriction\lambda} = \{v^1,\ldots,v^k \}$ una base de $E_\lambda$. Sigui $\mathcal{B} = \{v^1,\ldots,v^k,v^{k+1},\ldots,v^n \}$ una base d'$E$ obtinguda per extensió de $\B_{\restriction\lambda}$. Sigui $Q$ la matriu de canvi de base de ${\B}$ a la canònica:
		\begin{equation*}
			Q \defeq
			\begin{pmatrix}
			\vert&  &  \vert&  \vert&  &\vert  \\ 
			v^1_\mathcal{C}&  \cdots&  v^k_\mathcal{C}& v^{k+1}_\mathcal{C}  &  \cdots& v^n_\mathcal{C}  \\ 
			\vert&  &  \vert&  \vert&  &\vert
			\end{pmatrix}\,.
		\end{equation*}
		Siguin
		\begin{equation*}
			U \defeq 
			\begin{pmatrix}
				\vert           &        & \vert           \\
				v^1_\mathcal{C} & \cdots & v^k_\mathcal{C} \\
				\vert           &        & \vert
			\end{pmatrix}\in\matspace{n\times k}\,, \qquad
			V \defeq
			\begin{pmatrix}
				\vert               &        & \vert           \\
				v^{k+1}_\mathcal{C} & \cdots & v^n_\mathcal{C} \\
				\vert               &        & \vert
			\end{pmatrix} \in\matspace{n\times (n-k)}\,,
		\end{equation*}
		de manera que
		\[
			Q = 
			\left( 
			\begin{array}{c|c}
				U & V
			\end{array}
			\right)\,.
		\]
		
		$Q$ és invertible. Siguin $C\in\matspace{k\times n}$, $D\in\matspace{(n-k)\times n}$ tals que
		\begin{equation*}
			Q^{-1} = 
			\left( 
			\begin{array}{c}
				C  \\
				\hline
				D
			\end{array}
			\right)  \,.
		\end{equation*}
		Llavors tenim que
		\begin{equation*}
			\left( 
			\begin{array}{c|c}
				\Id_k& \varnothing  \\ 
				\varnothing& \Id_{n-k} 
			\end{array} 
			\right) =
			\Id_n = Q^{-1}Q = 
		\end{equation*}
	\end{proof}
\end{lema}

\section{Propietats de VAPs i VEPs}

\begin{lema}\label{lema:dvap-li}
	\begin{enumerate}[i)]
		\item $u,v$ VAP diferents l.i.
		\item Per VAPs $\lambda_1,\ldots,\lambda_r$ diferents, 
		\[\opdots{E_{\lambda_1}}{E_{\lambda_r}} = \opdots[\oplus]{E_{\lambda_1}}{E_{\lambda_r}}\,. \]
	\end{enumerate}
	\begin{proof}
		Siguin $g_1,\ldots, g_r\in\n$ les multiplicitats geomètriques de $\lambda_1,\ldots,\lambda_r$ respectivament. Siguin $B_1=\{v^1_1, v^1_2, \ldots, v^1_{g_1}\},\ldots,B_r=\{v^r_1, v^r_2, \ldots, v^r_{g_r}\}$ bases dels espais $E_{\lambda_1}, \ldots, E_{\lambda_r}$ respectivament. Serà suficient demostrar que els vectors de $\bigcup_{i=1}^r B_i$ són l.i. (noteu que $B_1,\ldots,B_r$ són necessàriament disjunts).
		
		Suposem que no ho són. Llavors, existeixen coeficients
		\begin{gather*}
			c^1_1,\ldots,c^1_{g_1} \in \k \\
			c^2_1,\ldots,c^2_{g_2} \in \k \\
			\vdots\\
			c^r_1,\ldots,c^r_{g_r}\in\k\,,
		\end{gather*}
		tals que 
		\begin{equation}\label{eq:nonzero-condition}
			\exists i \in \{1,\ldots,r\}\ \exists j \in \{1,\ldots,g_r\}\qcolon c^i_j \ne 0
		\end{equation}
		i
		\begin{equation}\label{eq:vepsld}
			\sum_{i=1}^{r} \sum_{j=1}^{g_r} c^i_j v^i_j = 0\,.
		\end{equation}
		Definim
		\begin{equation*}
			\forall i \in \{1,\ldots,r\}\quad u^i \defeq \sum_{j=1}^{g_r} c^i_j v^i_j\,.
		\end{equation*}
		Noteu que per tot $i$, $u^i \in E_{\lambda_i}$. Reescrivim \eqref{eq:vepsld} com
		\begin{equation}\label{eq:vepsld2}
			\sum_{i=1}^{r} u^i = 0\,.
		\end{equation}

		A partir de la condició \eqref{eq:nonzero-condition}, deduïm que $\exists i \in \{1,\ldots,r\}\colon u^i\ne 0$. 
		Sigui $\mathcal{N} \defeq \{i \mid u^i \ne 0 \} \subseteq \{1,\ldots, r\}$. A partir de (lema anterior), %TODO: add
		sabem que els vectors de $\{u^i \mid i\in\mathcal{N} \}$, que són VEPs de VAPs diferents, són l.i., i per tant
		\begin{equation*}
			\sum_{i\in\mathcal{N}} u^i \ne 0\,;
		\end{equation*}
		però això entra en contradicció directa amb \eqref{eq:vepsld2}.
		
		Per tant els vectors de $\bigcup_{i=1}^r B_i$ són l.i..
	\end{proof}
\end{lema}

\begin{col}\label{col:nVAPS}
	Si un endomorfisme $f$ d'un espai $E$ de dimensió $n$ té $n$ VAPs diferents $\subdots{\lambda}{n}\in\k$, llavors
	\[
		E = \opdots[\oplus]{E_{\lambda_1}}{E_{\lambda_n}}\,;
	\]
	ergo la unió de les bases dels espais propis forma una base d'$E$ i per tant $f$ diagonalitza.
	\begin{proof}
		Si tenim $n$ VAPs diferents, tenim $n$ espais propis diferents, $E_{\lambda_1}, E_{\lambda_2}, \ldots, E_{\lambda_n}$. Pel lema \ref{lema:geom-le-algeb}, sabem que $g_{\lambda_i} \ge 1$ per tot $i$. A més, pel lema \ref{lema:dvap-li},
		\[
			\dim\left(\sum_{i=1}^n E_{\lambda_i}\right)  = \sum_{i=1}^n\dim(E_{\lambda_i}) \ge \sum_{i=1}^n 1 = n = \dim E\,.
		\]
		D'altra banda, com que $\sum_i E_{\lambda_i} \subseteq E$, $\dim\left(\sum_{i=1}^n E_{\lambda_i}\right) \le \dim E$. Unint ambdues desigualtats obtenim que $\dim\left(\sum_{i=1}^n E_{\lambda_i}\right) = \dim E$, cosa que implica  $\sum_{i=1}^n E_{\lambda_i} = E$.
	\end{proof}
\end{col}

\begin{col}
	Si totes les arrels del polinomi característic $P_f(x)$ estan en $\k$ i són 
	simples (tenen multiplicitat única), llavors $f$ diagonalitza.
	\begin{proof}
		%TODO: F \subseteq E; dim F = dim E --> F = E
		Si totes les arrels estan en $\k$, llavors, pel teorema fonamental de l'àlgebra, en tenim $n$ (comptant multiplicitats). Si totes les arrels són simples, vol dir que totes $n$ arrels són diferents. Per tant $f$ té $n$ VAPs diferents. Pel corol·lari \ref{col:nVAPS} deduïm que $f$ diagonalitza.
	\end{proof}
\end{col}

\begin{teo}[Teorema de diagonalització]
	$f$ diagonalitza sii es compleixen les dues següents condicions:
	\begin{enumerate}[i)]
		\item El polinomi característic es pot descompondre completament en $\k$
		\item Per tot VAP $\lambda\in\k$ de $f$, $g_\lambda = a_\lambda$
	\end{enumerate}
	\begin{proof}
		Suposem existeix una base $\mathcal{V} = \{v_1,\ldots,v_n\}$ tal que $M_\mathcal{V}(f)$ és diagonal. Siguin $\lambda_1,\ldots,\lambda_r\in\k$ els VAPs i $m_1,\ldots,m_r$ les seves multiplicitats, respectivament, de manera que $v_1,\ldots,v_{m_1}$ són VEPs de VAP $\lambda_1$, \textellipsis, i $v_{n - m_r + 1},\ldots,v_{m_r}$ VEPs de VAP $\lambda_r$.
		\begin{equation*}
		M_\mathcal{V}(f) =
		\begin{pmatrix}
			\lambda_1 &        &           &           &        &           \\
			          & \ddots &           &           &        &           \\
			          &        & \lambda_1 &           &        &           \\
			          &        &           & \lambda_r &        &           \\
			          &        &           &           & \ddots &           \\
			          &        &           &           &        & \lambda_r
		\end{pmatrix}
		\end{equation*}
		Llavors $P_f(x) = \det(M_\mathcal{V}(f) - x\Id) = (\lambda_1 - x)^{m_1} \cdots  
		(\lambda_r - x)^{m_r}$; per tant totes les arrels del característic estan en $\k$. A més, per hipòtesi, $g_{\lambda_i} = m_i$ per tot $i\in[r]$.
		
		Ara demostrem la implicació recíproca. Per hipòtesi, $g_{\lambda_i} = a_{\lambda_i}$ per tot $i$. Llavors, pel lema \ref{lema:dvap-li},
		\[
			\dim\left(\sum_{i=1}^n E_{\lambda_i}\right)  = \sum_{i=1}^n\dim(E_{\lambda_i}) = \sum_{i=1}^n g_{\lambda_i} = \sum_{i=1}^n a_{\lambda_i}\,.
		\]
		Pel teorema fonamental de l'àlgebra, $\sum_{i=1}^n a_{\lambda_i} = n$; llavors concloem que
		\[
			\dim\left(\sum_{i=1}^n E_{\lambda_i}\right) = n = \dim E \ \Leftrightarrow\ \sum_{i=1}^n E_{\lambda_i} = E\,;
		\]
		ergo els VEPs formen base.
	\end{proof}
\end{teo}

\begin{enumerate}
	\item Pol característc
	\item Arrels; si estan en cos continuem
	\item Multiplicitat algebraica
	\item Multiplicitat geomètrica
	\item Comparem
	\item La matriu diagonal és la diagonal dels VEP
	\item La base és la unió de les bases de $E_{\lambda_i}$
\end{enumerate}

