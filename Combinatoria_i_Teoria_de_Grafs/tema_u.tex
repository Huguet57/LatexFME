\chapter{Geometries finites i quadrats llatins}
\section{Sistemes de representants distints}

\begin{defi}
    Sigui $X$ un conjunt finit i siguin $A_1, \dots, A_n\subseteq X$. Diem que la tupla $\lp a_1, \dots, a_n\rp$ és un sistema de representants distints (SRD) dels conjunts $A_1, \dots, A_n$ si $a_i\in A_i$.
    
    Sigui $J\subseteq \lc 1, \dots, n\rc$. Denotarem
    \[
        A\lp J\rp = \bigcup_{j\in J} A_j.
    \]
\end{defi}
\begin{defi}\label{defi_condicio_hall}
    Sigui $X$ un conjunt finit i siguin $A_1, \dots, A_n\subseteq X$. Diem que $A_1, \dots, A_n$ satisfan la condició de Hall (CH) si, per qualsevol $J\subseteq \lc 1, \dots, n\rc$, $\abs{A\lp J\rp} \geq \abs{J}$.
\end{defi}
\begin{teonom}[Teorema de Hall]
    Sigui $X$ un conjunt finit. Els subconjunts $A_1, \dots, A_n\subseteq X$ admeten un SRD si, i només si, satisfan la \hyperref[defi_condicio_hall]{CH}.
\end{teonom}
\begin{proof}
    La implicació directa és trivial. Demostrarem la recíproca per inducció sobre $n$. En cas base també és trivial. Per demostrar el pas inductiu, distingim entre dos casos
    \begin{enumerate}[(a)]
        \item $\nexists J, \, \varnothing \neq J \subsetneq \lc 1, \dots, n\rc$ tal que $\abs{A\lp J\rp} = \abs{J}$.
            
            Com que $\abs{A_n} = \abs{A\lp\lc n\rc\rp} \geq \abs{\lc n\rc} = 1$, existeix algun $a_n\in A_n$. Siguin $A_i^{\prime} = A_i \setminus \lc a_n\rc$. Per qualsevol $K\subseteq \lc 1, \dots, n-1\rc$, $K\neq \varnothing$, sigui $A^{\prime}\lp K\rp = \bigcup_{k\in K} A^{\prime}_k$. Es té que
            \[
                \abs{A^{\prime}\lp K\rp} \geq \abs{A\lp K\rp} - 1 \geq \abs{K} + 1 - 1 = \abs{K}.
            \]
            En virtut de la hipòtesi d'inducció, els conjunts $A_1^{\prime}, \dots, A_{n-1}^{\prime}$ admeten un SRD $\lp a_1, \dots, a_{n-1}\rp$. Per tant, $\lp a_1, \dots, a_n\rp$ és un SRD dels conjunts $A_1, \dots, A_n$.
        \item $\exists J, \, \varnothing \neq J \subsetneq \lc 1, \dots, n\rc$ tal que $\abs{A\lp J\rp} = \abs{J}$.
        
            Siguin $A_i^* = A_i\setminus A\lp J\rp$. Per qualsevol $K\subseteq \lc 1, \dots, n\rc \setminus J$, $K\neq \varnothing$, sigui 
            \[
                A^* \lp K\rp = \bigcup_{i\in K} A^*_i = A\lp J\cup K\rp \setminus A\lp J\rp.
            \]
            Es té que
            \[
                \abs{A^*\lp K\rp} = \abs{A\lp J\cup K\rp} - \abs{A\lp J\rp} \geq \abs{J\cup K} - \abs{J} = K.
            \]
            En virtut de la hipòtesi d'inducció, els conjunts $\lc A_i^* \mid i\in \lc 1, \dots, n\rc \setminus J\rc$ admeten un SRD $\lp a_1, \dots, a_r\rp$. Com que $J\neq \lc 1, \dots, n\rc$, podem tornar a fer servir la hipòtesi d'inducció obtenint que els conjunts $\lc A_j \mid j\in J\rc$ admeten un SRD $\lp a_1, \dots, a_s\rp$. Per com hem construït els conjunts $A_i^*$, els representants $\lp a_1, \dots, a_r\rp$ i $\lp a_1, \dots, a_s\rp$ són tots distints, de manera que llur unió (adequadament ordenada) forma una tupla $\lp a_1, \dots, a_n\rp$ que és SRD dels conjunts $A_1, \dots, A_n$.
    \end{enumerate}
\end{proof}

\begin{teonom}[Teorema de Hall estès]
    Sigui $X$ un conjunt i siguin $A_1, \dots, A_n\subseteq X$. Si $r\leq n$, $\abs{A_i}\geq r, \, \forall 1\leq i\leq n$, i els conjunts satisfan la \hyperref[defi_condicio_hall]{CH}, aleshores existeixen, almenys, $r!$ SRD diferents.
\end{teonom}
\begin{proof}
    La demostració es pot fer sobre l'esquema de la demostració anterior. El cas base de la inducció és trivial. Pel pas inductiu, distingim entre els dos mateixos casos.
    \begin{enumerate}[(a)]
        \item $\nexists J, \, \varnothing \neq J \subsetneq \lc 1, \dots, n\rc$ tal que $\abs{A\lp J\rp} = \abs{J}$.
            
            Com que $\abs{A_n}\geq r$, podem triar un element $a_n\in A_n$, almenys, d'$r$ maneres diferents. Els conjunts $A_1^{\prime}, \dots, A_{n-1}^{\prime}$ són tals que $\abs{A_i^{\prime}} \geq \abs{A_i} - 1 \geq r-1$, de manera que tenen, almenys, $\lp r-1\rp !$ SRD diferents, en virtut de la hipòtesi d'inducció. Per tant, obtenim un total d'almenys $r!$ possibilitats.
        \item $\exists J, \, \varnothing \neq J \subsetneq \lc 1, \dots, n\rc$ tal que $\abs{A\lp J\rp} = \abs{J}$.
        
            En aquest cas, $r\leq \abs{A\lp J\rp} = \abs{J} < n$. En virtut de la hipòtesi d'inducció, els conjunts $\lc A_j \mid j\in J\rc$ tenen, almenys, $r!$ SRD diferents, de manera que els conjunts $A_1, \dots, A_n$ també en tenen, almenys, $r!$.
    \end{enumerate}
\end{proof}
